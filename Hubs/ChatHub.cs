﻿//using System;
//namespace ecommerce_server.Hubs
//{
//    public class ChatHub
//    {
//        public ChatHub()
//        {
//        }
//    }
//}

using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace ecommerce_server.Hubs
{
    public class ChatHub : Hub
    {
        public async Task SendMessage(string user, string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", user, message);
        }

        public async Task Send(string user, string message)
        {
            // Call the "OnMessage" method to update clients.
            await Clients.All.SendCoreAsync("OnMessage", new object[] { user, message });
        }
    }

}