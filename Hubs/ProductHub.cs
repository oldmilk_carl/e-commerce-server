﻿//using System;
//namespace ecommerce_server.Hubs
//{
//    public class ChatHub
//    {
//        public ChatHub()
//        {
//        }
//    }
//}

using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using LiteDB;

namespace ecommerce_server.Hubs
{
    public class ProductHub : Hub
    {

        //Dictionary<string, Product> productDic = new Dictionary<string, Product>();

        public async Task Add(string name, string description, int categoryId, int maxStock, string price, string ownerId)
        {
            //await Clients.All.SendAsync("OnProductAdded", name, description, int maxStock, double price, string owner);

            string pid = System.Guid.NewGuid().ToString();
            Product product = new Product();
            product.pid = pid;
            product.name = name;
            product.description = description;
            product.categoryId = categoryId;
            product.maxStock = maxStock;
            product.currentStock = maxStock;
            product.price = price;
            product.ownerId = ownerId;

            //db
            // Open database (or create if doesn't exist)
            using (var db = new LiteDatabase(@"Data.db"))
            {
                // Get a collection (or create, if doesn't exist)
                var col = db.GetCollection<Product>("products");

                // Index document using document Name property
                //col.EnsureIndex(x => x.pid);


                 col.Insert(product);
            }
            await Clients.All.SendCoreAsync("OnProductAdded", new object[] { product });
        }

        public async Task Remove(string pid)
        {
            using (var db = new LiteDatabase(@"Data.db"))
            {
                // Get a collection (or create, if doesn't exist)
                var col = db.GetCollection<Product>("products");

                // Index document using document Name property
                col.EnsureIndex(x => x.pid);

                col.DeleteMany(x => x.pid == pid);
            }

            await Clients.All.SendCoreAsync("OnProductRemoved", new object[] { pid });
        }

        public async Task Update(string pid, string name, string description, int maxStock, string price)
        {
            

            Product doc;
            using (var db = new LiteDatabase(@"Data.db"))
            {
                // Get a collection (or create, if doesn't exist)
                var col = db.GetCollection<Product>("products");

                // Index document using document Name property
                col.EnsureIndex(x => x.pid);

                doc = col.FindOne(x => x.pid == pid);
                doc.name = name;
                doc.description = description;
                doc.maxStock = maxStock;
                doc.price = price;

                col.Update(doc);

            }


            await Clients.All.SendCoreAsync("OnProductUpdated", new object[] { doc });
        }

        public async Task Refresh(/*bool dump*/) {

            Product[] products;
            using (var db = new LiteDatabase(@"Data.db"))
            {
                // Get a collection (or create, if doesn't exist)
                var col = db.GetCollection<Product>("products");

                //if (dump) {

                //    string pid = System.Guid.NewGuid().ToString();
                //    Product product = new Product();
                //    product.pid = pid;
                //    product.name = "NAME";
                //    product.description = "DES";
                //    product.categoryId = 1;
                //    product.maxStock = 10;
                //    product.currentStock = 10;
                //    product.price = "10.00";
                //    product.ownerId = "SYSTEM";

                //    col.Insert(product);
                //}


                // Index document using document Name property
                col.EnsureIndex(x => x.pid);


                products = col.FindAll().ToArray();
                
            }

            await Clients.All.SendCoreAsync("OnProductRefreshed", products);
        }
    }

    class Product {
        public int Id { get; set; }
        public string pid { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int categoryId { get; set; }
        public int maxStock { get; set; }
        public int currentStock { get; set; }
        public string price { get; set; }
        public string ownerId { get; set; }
    }

}