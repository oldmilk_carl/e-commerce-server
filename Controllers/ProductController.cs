﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ecommerce_server.Hubs;
using LiteDB;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ecommerce_server.Controllers
{
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        private readonly IHubContext<ProductHub> _hubContext;
        public ProductController(IHubContext<ProductHub> hubContext)
        {
            _hubContext = hubContext;
        }

        // PUT api/values/5
        [HttpPut("{pid}")]
        public async void Update(string pid, string price)
        {
            Product doc;
            using (var db = new LiteDatabase(@"Data.db"))
            {
                // Get a collection (or create, if doesn't exist)
                var col = db.GetCollection<Product>("products");

                // Index document using document Name property
                col.EnsureIndex(x => x.pid);

                doc = col.FindOne(x => x.pid == pid);
                if (doc != null) {
                    doc.price = price;
                    col.Update(doc);

                    await _hubContext.Clients.All.SendCoreAsync("OnProductUpdated", new object[] { doc });
                }
            }
            //return value.ToString();
        }

    }
}
